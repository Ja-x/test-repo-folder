import { useState, useEffect } from 'react';
import './App.css'
import { Keypad } from './Keypad.jsx'

function App() {
  const [secret, setSecret] = useState('');

  useEffect(() => {
    const generateSecret = () => {
      const a = Math.floor(Math.random() * 10000).toString().padStart(4, '0');
      setSecret(a);
    };
    generateSecret();
  }, []);

  return (
    <>
      <div>
      </div>
      <h1 className='mainTitle'>GCAQVZ3</h1>
      <h2 className='h2Title'>Solve the code</h2>
        <div >
          <Keypad secret={secret} />
        </div>
    </>
  )
}

export default App
